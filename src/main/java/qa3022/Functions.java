package qa3022;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class Functions extends WebDriver {
    public void CheckTitle(WebElement element, String value){
        Assert.assertEquals(element.getText(),value);

    }
    public void WaitPageLoad(){
        driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);

    }
}
