package qa3022;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Objects extends Functions {

    @FindBy(name = "name")
    public WebElement u_name;

    @FindBy(name = "surname")
    public WebElement u_surname;

    @FindBy(name = "patronymic")
    public WebElement u_patronymic;

    @FindBy(name = "phone")
    public WebElement u_phone;

    @FindBy(name = "email")
    public WebElement u_mail;

    @FindBy(name = "message")
    public WebElement u_text;

    @FindBy(name = "agree")
    public WebElement u_agree;

    @FindBy(className = "Button__content")
    public WebElement u_button;

    @FindBy(xpath = "/html/body/div[3]/div/div[1]/div[2]/div/div/div/h1")
    public WebElement finish_title;
}
