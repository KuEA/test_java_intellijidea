package qa3022;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;

public class WebDriver extends Params {



/*
    @Before
    public void InitDriver() {
        System.setProperty("webdriver.chrome.driver", "C:/Users/User/Desktop/Katalon/Katalon_Studio_Windows_64/configuration/resources/temp/webdriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

    }*/


    @Before
    public void InitDocker() throws MalformedURLException {

        if(docker_switch=="ON") {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        capabilities.setVersion("78.0");
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);
        capabilities.setCapability("browserSize","1920x1080");

        driver = new RemoteWebDriver(
                URI.create("https://katalon-qa.mos-team.ru:8080/wd/hub/").toURL(),
                capabilities

        );
        }
        else {
            System.setProperty("webdriver.chrome.driver", "C:/Users/User/Desktop/Katalon/Katalon_Studio_Windows_64/configuration/resources/temp/webdriver/chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }


    }

    @After
    public void CloseBrowser() {
           driver.close();
    }
}
