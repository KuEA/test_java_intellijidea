package qa3022;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Params {
    public RemoteWebDriver driver;
    public String[] data = new String[] {"Иван","Иванов","Иваныч","88005553535","ivanovivanstar@mail.ru","Отличный сайт! Огромное спасибо за него!"};
    public String[] data1 = new String[] {"Петр","Петров","Петрович","85189677889","pituh123@mail.ru","Мне не нравится ваш сайт! Пиво лучше!"};
    public String name, surname, patronymic, phone, mail, text;
    public String flag_title = "Предложение отправлено";
    public String docker_switch = "ON"; // Для запуска в докере значение ON, для локали - любое другое

}
