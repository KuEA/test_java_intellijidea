package TestSuits;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import qa3022.Objects;

public class AnotherTests extends Objects {

    @Test
    public void StepThree() throws InterruptedException {


        driver.get("https://www.mos.ru/feedback/idea/");
        PageFactory.initElements(driver,  this);
        System.out.println(driver.getTitle());
        WaitPageLoad();
        name = data[0];
        surname = data[1];
        patronymic = data[2];
        phone = data[3];
        mail = data[4];
        text = data[5];
        u_surname.sendKeys(surname);
        u_name.sendKeys(name);
        u_patronymic.sendKeys(patronymic);
        u_phone.sendKeys(phone);
        u_mail.sendKeys(mail);
        u_text.sendKeys(text);
        u_agree.click();
        u_button.click();
        Thread.sleep(5000);
        CheckTitle(finish_title,flag_title);
    }
}
