package TestSuits;


import kotlin.Suppress;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import qa3022.Objects;

import java.util.concurrent.TimeUnit;


public class Tests extends Objects {


    @Test
    public void StepOne() throws InterruptedException {


        driver.get("https://www.mos.ru/feedback/idea/");
        PageFactory.initElements(driver,  this);
        System.out.println(driver.getTitle());
        WaitPageLoad();
        name = data[0];
        surname = data[1];
        patronymic = data[2];
        phone = data[3];
        mail = data[4];
        text = data[5];
        u_surname.sendKeys(surname);
        u_name.sendKeys(name);
        u_patronymic.sendKeys(patronymic);
        u_phone.sendKeys(phone);
        u_mail.sendKeys(mail);
        u_text.sendKeys(text);
        u_agree.click();
        u_button.click();
        Thread.sleep(5000);
        CheckTitle(finish_title,flag_title);
    }


    @Test
    public void StepTwo() throws InterruptedException {


        driver.get("https://www.mos.ru/feedback/idea/");
        System.out.println(driver.getTitle());
        driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
        name = data1[0];
        surname = data1[1];
        patronymic = data1[2];
        phone = data1[3];
        mail = data1[4];
        text = data1[5];
        WebElement el = driver.findElementByName("surname");
        el.sendKeys(surname);
        driver.findElementByName("name").sendKeys(name);
        driver.findElementByName("patronymic").sendKeys(patronymic);
        driver.findElementByName("phone").sendKeys(phone);
        driver.findElementByName("email").sendKeys(mail);
        driver.findElementByName("message").sendKeys(text);
        driver.findElementByName("agree").click();
        driver.findElementByClassName("Button__content").click();
        Thread.sleep(5000);
        String flag = driver.findElementByXPath("/html/body/div[3]/div/div[1]/div[2]/div/div/div/h1").getText();
        Assert.assertEquals(flag,"Предложение отправлено");
    }


}

